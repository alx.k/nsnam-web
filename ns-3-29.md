---
layout: page
title: ns-3.29
permalink: /releases/ns-3-29/
---

This ns-3 release is dedicated to the memory of project co-founder [George Riley](https://www.ece.gatech.edu/news/607255/loving-memory-george-riley).

**ns-3.29** was released on September 4, 2018, with the following documentation:
*   The ns-3.29 **tutorial** is available in [HTML](/docs/release/3.29/tutorial/html/index.html), [HTML (single page)](/docs/release/3.29/tutorial/singlehtml/index.html), and [PDF](/docs/release/3.29/tutorial/ns-3-tutorial.pdf).
*   The ns-3.29 **reference manual** is available in [HTML](/docs/release/3.29/manual/html/index.html), [HTML (single page)](/docs/release/3.29/manual/singlehtml/index.html), and [PDF](/docs/release/3.29/manual/ns-3-manual.pdf).
*   The ns-3.29 **model library documentation** is available in [HTML](/docs/release/3.29/models/html/index.html), [HTML (single page)](/docs/release/3.29/models/singlehtml/index.html), and [PDF](/docs/release/3.29/models/ns-3-model-library.pdf).
*   **Doxygen** documentation [HTML](/docs/release/3.29/doxygen/)

ns-3.29 includes the following new features:

*   An HTTP model based on a 3GPP reference model for HTTP/1.1
*   A priority queue disc (PrioQueueDisc) for the traffic control module
*   A model for the TCP Proportional Rate Reduction (PRR) recovery algorithm
*   A node position allocator that rejects positions that are located within buildings defined in the scenario

Finally, the release includes numerous bug fixes and small improvements, listed in the [RELEASE\_NOTES](http://code.nsnam.org/ns-3.29/file/078dcf663058/RELEASE_NOTES).

*   The latest ns-3.29 release source code can be downloaded from [here](https://www.nsnam.org/release/ns-allinone-3.29.tar.bz2)
*   What has changed since ns-3.28? Consult the [changes](http://code.nsnam.org/ns-3.29/file/078dcf663058/CHANGES.html) page.
*   Errata containing late-breaking information about the release can be found [here](http://www.nsnam.org/wiki/index.php/Errata)
*   A patch to upgrade from ns-3.28.1 to the updated ns-3.29 can be found [here](https://www.nsnam.org/release/patches/ns-3.28.1-to-ns-3.29.patch)

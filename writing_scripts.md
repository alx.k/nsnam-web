---
layout: page
title: Writing Scripts
permalink: /support/faq/writing-scripts/
---

Starting from an existing script
While experienced users of ns-3 often write new simulation scripts from scratch, most users merely copy/paste an existing script which incorporates the simulation models they need and modify it until it matches what they want. Consult the extensive example scripts that ship with every module.

Building a new C++ script
Once you have identified which simulation script you would like to start from, building a modified version of this script is a matter of dropping it in the scratch directory and running waf again:

```bash
cp examples/csma/csma-broadcast.cc scratch/csma-modified.cc
./waf
```
Running a new script which was dropped in the scratch directory is similar to running other examples.

Building many C++ files
If your simulation script becomes complex and if you split it in more than one C++ source file, you need to create a new sub-directory in the scratch directory:

```bash
mkdir scratch/modified
cp x.cc scratch/modified
cp y.cc scratch/modified
./waf
```
This will build a new program named after the name of your sub-directory (modified here) and you can run it just like any other example:

```bash
./waf --run modified
```

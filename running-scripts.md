---
layout: page
title: Running Scripts
permalink: /support/faq/running-scripts/
---

## Running C++ scripts
Once the build is done and all tests pass, one generally wants to run a couple of examples as per the tutorial. The easiest way to do this is to run:

```bash
./waf --run progname
```

To find out the list of available programs, you can do:

```bash
./waf --run non-existent-program-name
```

## Running python scripts
The method to run a python script is fairly similar to that of a C++ script except that you need to specify a complete path to the script file, and you need to use the –pyrun command instead of –run. For example:

```bash
./waf --pyrun examples/wireless/mixed-wireless.py
```

## Running scripts in a shell
Another way to run ns-3 programs that does not require using the ./waf –run command is to use the ns-3 shell which takes care of setting up all the environment variables necessary to do so:

```bash
./waf shell
```

And, then:

```bash
./build/debug/examples/csma-broadcast
```

## Running C++ scripts with gdb
If you do not use the ns-3 shell, and if you want to run your simulation under a special tool (valgrind or gdb), you need to use a command template:

```bash
./waf --run csma-cd-one-subnet --command-template="gdb %s"
```

or:

```bash
./waf --run csma-cd-one-subnet --command-template="valgrind %s"
```
